
G1.filename = '../../data/zeros.dat';
G1.err = 3.84358;

G2.filename = '../../data/ones.dat';
G2.err = 2.90237;


more off;

disp(['loading data from ' G1.filename '...']);
G1.dat = load(G1.filename);

disp(['loading data from ' G2.filename '...']);
G2.dat = load(G2.filename);

e.err0 = 3.84358;
e.err1 = 2.90237;

G = aggregate(G1, G2, e);
