more off;
N = 200;
#rand("seed", 100);
r = rand(N,1);
e.err0 = norm(r)/sqrt(N);
e.err1 = norm(1-r)/sqrt(N);

G1.dat = rand(N,1);
G2.dat = rand(N,1);
G1.err = norm(G1.dat-r)/sqrt(N);
G2.err = norm(G2.dat-r)/sqrt(N);

g = [ ones(N,1) G1.dat G2.dat ];
w = inv((g' * g)) * g' * r;

G = aggregate(G1, G2, e);
disp("gradient descent results:");
disp("weights:");
disp(G.w);
disp("error:");
disp(G.err);

disp("linear regression results:");
disp("weights:");
disp(w);
disp("error:");
disp(norm(w(1) + w(2) * G1.dat + w(3) * G2.dat - r)/sqrt(N));
