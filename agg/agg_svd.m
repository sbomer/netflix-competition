filename{1} = '../../results/svdpp_train';
filename{2} = '../../results/svdpp_trainQual'; 
filename{3} = '../../results/svdpp_trainProbe';
filename{4} = '../../results/svdpp_all';
filename{5} = '../../results/tsvdpp_but_train';
filename{6} = '../../results/tsvdpp_but_trainQual';
filename{7} = '../../results/tsvdpp_but_trainProbe';
filename{8} = '../../results/tsvdpp_but_all';
filename{9} = '../../results/tsvdpp_bitbin_trainQual';
filename{10} = '../../results/tsvdpp_bitbin_all';
filename{11} = '../../results/tsvdpp_au_trainQual';
filename{12} = '../../results/tsvdpp_au_all_30';
filename{13} = '../../results/tsvdpp_apu_train';
filename{14} = '../../results/tsvdpp_apu_10';
filename{15} = '../../results/new';

e0 = 3.84358;
eg(1) = 2.90237;

more off;
for k=1:length(filename)
  disp(['loading data from ' filename{k}]);
  eg(k+1) = load([filename{k} '/err']);
  disp(['err: ' num2str(eg(k+1))]);
  G(:,k+1) = load([filename{k} '/predictions.dat']);
end

G(:,1) = ones(size(G, 1), 1);

[w, g] = aggregate_n(G, eg, e0);
