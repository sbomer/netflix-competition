function [G] = aggregate (G1, G2, e)

N = length(G1.dat);
E0 = N * e.err0^2;
E1 = N * e.err1^2;
Eg1 = N * G1.err^2;
Eg2 = N * G2.err^2;
g1 = G1.dat;
g2 = G2.dat;

w0 = 0;
w1 = 0;
w2 = 0;

lrate = 1/(50*N);

E = 0;
do
disp('following error gradient...');

tw0 = w0;
tw1 = w1;
tw2 = w2;

w0 -= lrate * (2*N*tw0 + 2*tw1*sum(g1) + 2*tw2*sum(g2) - (N+E0-E1));
w1 -= lrate * (2*tw1*norm(g1)^2 + 2*tw0*sum(g1) + 2*tw2*dot(g1,g2) - (norm(g1)^2+E0-Eg1));
w2 -= lrate * (2*tw2*norm(g2)^2 + 2*tw0*sum(g2) + 2*tw1*dot(g1,g2) - (norm(g2)^2+E0-Eg2));

prevE = E;
E = N * w0^2 + w1^2*norm(g1)^2 + w2^2*norm(g2)^2 + E0 + 2*w0*w1*sum(g1) + 2*w0*w2*sum(g2) + 2*w1*w2*dot(g1,g2) - w0*(N+E0-E1) - w1*(norm(g1)^2+E0-Eg1) - w2*(norm(g2)^2+E0-Eg2);
err = sqrt(E/N)
until abs(E-prevE) < 1e-8;

G.dat = w0 + w1 * g1 + w2 * g2;
G.err = err;
G.w = [w0; w1; w2];

endfunction
