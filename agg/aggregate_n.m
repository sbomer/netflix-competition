function [w, g] = aggregate_n (G, eg, e0)

N = size(G, 1);
E0 = N * e0^2;
Eg = N * eg.^2;
Sgr = (sum(G.^2) + E0 - Eg)' / 2;
w = pinv(G'*G)*Sgr;
g = G * w;

endfunction
