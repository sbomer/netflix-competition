#include "NetflixData.hh"
#include <iterator>
#include <algorithm>
#include <cassert>

using namespace std;

const int NetflixData::RATINGS = 102416306;
const int NetflixData::USERS = 458293;
const int NetflixData::MOVIES = 17770;
const int NetflixData::DAYS = 2243;

bool userComp(const Datum &a, const Datum &b) {
  return a.user < b.user;
}

bool movieComp(const Datum &a, const Datum &b) {
  return a.movie < b.movie;
}

bool dateComp(const Datum &a, const Datum &b) {
  return a.day < b.day;
}

ostream& operator<< (ostream& os, const Datum& d) {
  os << d.user << " " << d.movie << " "  << d.day << " " << int(d.rating);
  return os;
}

istream& operator>> (istream& is, Datum& d) {
  int rating;
  is >> d.user >> d.movie >> d.day >> rating;
  d.rating = char(rating);
  return is;
}

NetflixData::NetflixData() {};

NetflixData::NetflixData(string filename) {
  load(filename);
}

vector<Datum>::size_type NetflixData::size() const {
  return data.size();
}

vector<Datum>::const_iterator NetflixData::begin() const {
  return data.begin();
}

vector<Datum>::const_iterator NetflixData::end() const {
  return data.end();
}

vector<Datum>::const_iterator NetflixData::userBegin(int u) const {
  return userIt[u-1];
}

vector<Datum>::const_iterator NetflixData::userEnd(int u) const {
  return userIt[u];
}

istream &operator>>(istream &is, NetflixData &nd) {
  nd.numUsers = 0;
  nd.numMovies = 0;
  nd.numDays = 0;
  nd.numRatings = 0;
  nd.userIt.clear();
  nd.userIt.reserve(NetflixData::USERS);
  nd.data.clear();
  nd.data.reserve(NetflixData::RATINGS);
  Datum d;
  int u = 0;
  while (is >> d) {
    if (d.user != u) {
      assert(d.user > u);
      while (u != d.user) {
	nd.userIt.push_back(nd.data.end());
	++u;
      }
    }
    nd.numUsers = max(nd.numUsers, d.user);
    nd.numMovies = max(nd.numMovies, d.movie);
    nd.numDays = max(nd.numDays, d.day);
    if (d.rating != 0) {
      ++nd.numRatings;
    }
    nd.data.push_back(d);
  }
  nd.userIt.push_back(nd.data.end());
  return is;
}

void NetflixData::load(string filename) {
  ifstream is(filename.c_str());
  if (!is) {
    cout << "error loading from " << filename << endl;
    exit(1);
  }
  cout << "loading data from " << filename << "..." << endl;
  is >> *this;
}

void NetflixData::save(string filename) {
  ofstream os(filename.c_str());
  cout << "saving data to " << filename << "..." << endl;
  os << *this;
}

ostream &operator<< (ostream &os, const NetflixData &nd) {
  ostream_iterator<Datum> it (os, "\n");
  copy(nd.begin(), nd.end(), it);
  return os;
}

void NetflixData::userSort() {
  cout << "sorting data by user..." << endl;
  sort(data.begin(), data.end(), userComp);
}

void NetflixData::movieSort() {
  cout << "sorting data by movie..." << endl;
  sort(data.begin(), data.end(), movieComp);
}

void NetflixData::dateSort() {
  cout << "sorting data by date..." << endl;
  sort(data.begin(), data.end(), dateComp);
}

void NetflixData::loadFromInd(string allFilename, string indFilename, bool (*accept)(const Datum &d, int i)) {
  numUsers = 0;
  numMovies = 0;
  numDays = 0;
  numRatings = 0;
  userIt.clear();
  userIt.reserve(NetflixData::USERS);
  ifstream allFile(allFilename.c_str());
  ifstream indFile(indFilename.c_str());
  data.clear();
  data.reserve(NetflixData::RATINGS);
  Datum d;
  int i;
  int u = 0;
  cout << "loading indexed data from " << allFilename << " and " << indFilename << "..." << endl;
  while (allFile >> d && indFile >> i) {
    if (accept(d, i)) {
      if (d.user != u) {
	userIt.push_back(data.end());
	u = d.user;
      } 
      numUsers = max(numUsers, d.user);
      numMovies = max(numMovies, d.movie);
      numDays = max(numDays, d.day);
      if (d.rating != 0) {
	++numRatings;
      }
      data.push_back(d);
    }
  }
}
