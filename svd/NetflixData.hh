#ifndef NETFLIX_DATA_HH
#define NETFLIX_DATA_HH

#include <vector>
#include <iostream>
#include <fstream>

struct Datum {
  int user, movie;
  short int day;
  char rating;
};

bool userComp(const Datum&, const Datum&);
bool movieComp(const Datum&, const Datum&);
bool dateComp(const Datum&, const Datum&);

std::ostream& operator<< (std::ostream&, const Datum&);
std::istream& operator>> (std::istream&, Datum&);
 
class NetflixData {
  std::vector<Datum> data;
  std::vector<std::vector<Datum>::const_iterator> userIt;
  int numUsers;
  int numMovies;
  short int numDays;
  int numRatings;
public:
  static const int RATINGS, USERS, MOVIES, DAYS;
  NetflixData();
  NetflixData(std::string);
  std::vector<Datum>::size_type size() const;
  std::vector<Datum>::const_iterator begin() const;
  std::vector<Datum>::const_iterator end() const;
  std::vector<Datum>::const_iterator userBegin(int) const;
  std::vector<Datum>::const_iterator userEnd(int) const;
  // i/o operations
  void loadText(std::ifstream&);
  void saveText(std::ofstream&);
  void loadBinary(std::ifstream&);
  void saveBinary(std::ofstream&);
  void userSort();
  void movieSort();
  void dateSort();
  int users() const;
  int movies() const;
  short int days() const;
  int ratings() const;
  int userSize(int) const;
  friend std::istream &operator>>(std::istream&, NetflixData &);
  void loadFromInd(std::string allFile, std::string indFile, bool (*accept)(const Datum &d, int i));
  void save(std::string);
  void load(std::string);
};

inline int NetflixData::users() const {
  return numUsers;
}

inline int NetflixData::movies() const {
  return numMovies;
}

inline short int NetflixData::days() const {
  return numDays;
}

inline int NetflixData::ratings() const {
  return numRatings;
}

inline int NetflixData::userSize(int u) const {
  return userEnd(u) - userBegin(u);
}

std::ostream &operator<<(std::ostream&, const NetflixData &);

#endif
