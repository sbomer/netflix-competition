#include "NetflixSVD.hh"
#include <time.h>
#include <cassert>

using namespace std;
using namespace Eigen;


NetflixSVD::NetflixSVD(const NetflixData &trainSet) : trainSet(trainSet) {
  //initialize learning rates and regularization constants

  //user bias
  lrate_b_u = .003; reg_b_u = .03;
  lrate_a_u = 0.00001; reg_a_u = 50;
  lrate_b_ut= 0.0025; reg_b_ut = 0.005;

  //movie bias
  lrate_b_i = .002; reg_b_i = .03;
  lrate_b_itbin = 0.00005; reg_b_itbin = 0.1;
  lrate_c_u = 0.008; reg_c_u = 0.01;
  lrate_c_ut = 0.002; reg_c_ut = 0.005;

  //user/movie features
  lrate_f = 0.008; reg_f = 0.015;

  //initialize other meta parameters
  timeBins = 30;
  features = 200;
  random(0.001);
  findMean();
  findMeanTimes();
  iter = 0;
};

VectorXf NetflixSVD::sumFactors(int user) const {
  VectorXf sum_y_j = VectorXf::Zero(features);
  for (vector<Datum>::const_iterator it = trainSet.userBegin(user); it != trainSet.userEnd(user); ++it) {
    sum_y_j += y_j.col(it->movie-1);
  }
  int points = trainSet.userSize(user);
  if (points > 0) {
    sum_y_j /= sqrt(points);
  }
  return sum_y_j;
}

vector<float> NetflixSVD::predict(const NetflixData &d) const {
  vector<float> predictions;
  predictions.reserve(d.size());
  for (int u=0; u != d.users(); ++u) {
    VectorXf factorSum = sumFactors(u+1);
    for (vector<Datum>::const_iterator it = d.userBegin(u+1); it != d.userEnd(u+1); ++it) {
      predictions.push_back(predict(*it, factorSum));
    }
  }
  return predictions;
}

inline float NetflixSVD::error(const Datum &d, const VectorXf &factorSum) const {
  return d.rating - predict(d, factorSum);
}

inline float NetflixSVD::predict(const Datum &d, const VectorXf &factorSum) const {
  int u = d.user-1;
  int i = d.movie-1;
  int t = d.day-1;
  int dev = timeDev(u+1, t+1);
  std::unordered_map<short int, float>::const_iterator it;

  //user bias
  //it = b_ut[u].find(t);
  //float user_bias = b_u(u) + a_u(u) * dev;
  //if (it != b_ut[u].end()) {
  //user_bias += it->second;
  //}
  float user_bias = b_u(u);

  //movie bias
  //float tmp_c_u = c_u(u);
  //it = c_ut[u].find(t);
  //float tmp_c_ut = it != c_ut[u].end() ? it->second : 0;
  //float movie_bias = (tmp_c_u + tmp_c_ut) * (b_i(i) + b_itbin(timeBin(t+1), i));
  //float movie_bias = b_i(i) + b_itbin(timeBin(t+1), i);
  float movie_bias = b_i(i);

  //user feature 
  //VectorXf user_feature = p_u.col(u) + ap_u.col(u) * dev + factorSum;
  VectorXf user_feature = p_u.col(u) + factorSum;
  //maybe later
  //  std::unordered_map<short int, VectorXf>::const_iterator vit;
  //  vit = p_ut[u].find(t);
  //if (vit != p_ut[u].end()) {
  //  user_feature += vit->second;
  //}

  return mean + user_bias + movie_bias + user_feature.dot(q_i.col(i));
}

void NetflixSVD::train() {
  ++iter;
  cout << "iteration " << iter << ":" << endl;
  cout << "training with " << features << " features..." << endl;
  clock_t start = clock();
  int users = trainSet.users();
  for (int u=0; u != users; ++u) {
    VectorXf sum_y_j = sumFactors(u+1);
    VectorXf sum_y_j_0 = sum_y_j;
    for (vector<Datum>::const_iterator it = trainSet.userBegin(u+1); it != trainSet.userEnd(u+1); ++it) {
      if (it->rating != 0) {
	float err = error(*it, sum_y_j);
	int i = it->movie-1;
	int t = it->day-1;

	//user bias
	b_u(u) += lrate_b_u * (err - reg_b_u * b_u(u));
	//a_u(u) += lrate_a_u * (err * timeDev(u+1, t+1) - reg_a_u * a_u(u));
	//b_ut[u][t] += lrate_b_ut * (err - reg_b_ut * b_ut[u][t]);
	
	//movie bias
	float tmp_b_i = b_i(i);
	//int bin = timeBin(t+1);
	//float tmp_b_itbin = b_itbin(bin, i);
	//float tmp_c_u = c_u(u);
	//float tmp_c_ut = c_ut[u][t];
	//b_i(i) += lrate_b_i * (err * (tmp_c_u + tmp_c_ut) - reg_b_i * tmp_b_i);
	//b_itbin(bin, i) += lrate_b_itbin * (err * (tmp_c_u + tmp_c_ut) - reg_b_itbin * tmp_b_itbin);
	//b_itbin(bin, i) += lrate_b_itbin * (err - reg_b_itbin * tmp_b_itbin);
	//c_u(u) += lrate_c_u * (err * (tmp_b_i + tmp_b_itbin) - reg_c_u * (tmp_c_u-1));
	//c_ut[u][t] += lrate_c_ut * (err * (tmp_b_i + tmp_b_itbin) - reg_c_ut * tmp_c_ut);
	b_i(i) += lrate_b_i * (err - reg_b_i * tmp_b_i);
	
	//user/movie features
	VectorXf tmp_sum_y_j = sum_y_j;
	VectorXf tmp_p_u = p_u.col(u);
	//VectorXf tmp_ap_u = ap_u.col(u);
	//maybe later
	////VectorXf tmp_p_ut = p_ut[u][t];
 	VectorXf tmp_q_i = q_i.col(i);
	sum_y_j += lrate_f * (err * tmp_q_i - reg_f * tmp_sum_y_j);
	p_u.col(u) += lrate_f * (err * tmp_q_i - reg_f * tmp_p_u);
	//ap_u.col(u) += lrate_a_u * (err * timeDev(u+1, t+1) * tmp_q_i - reg_a_u * tmp_ap_u);
	////p_ut[u][t] += uTBLRate * (err * tmp_q_i - uTBReg * tmp_p_ut);
	//q_i.col(i) += lrate_f * (err * (tmp_p_u + tmp_ap_u * timeDev(u+1, t+1) + tmp_sum_y_j) - reg_f * tmp_q_i);
	q_i.col(i) += lrate_f * (err * (tmp_p_u + sum_y_j) - reg_f * tmp_q_i);
	////q_i.col(i) += lrate_f * (err * (tmp_p_u + tmp_ap_u * timeDev(u+1, t+1) + tmp_p_ut + tmp_sum_y_j) - reg_f * tmp_q_i);
      }
    }
    for (vector<Datum>::const_iterator it = trainSet.userBegin(u+1); it != trainSet.userEnd(u+1); ++it) {
      y_j.col(it->movie-1) += (sum_y_j - sum_y_j_0) / sqrt(trainSet.userSize(u+1));
    }
  }
  cout << "finished in " << float(clock() - start) / CLOCKS_PER_SEC << " seconds" << endl;
}

void NetflixSVD::random(float range) {
  //user bias
  cout << "initializing user bias (b_u) vector..." << endl;
  b_u = VectorXf::Random(trainSet.users()) * range;
  //cout << "initializing user time factor (a_u) vector..." << endl;
  //a_u = VectorXf::Random(trainSet.users()) * range;

  //movie bias
  cout << "initializing movie bias (b_i) vector..." << endl;
  b_i = VectorXf::Random(trainSet.movies()) * range;
  //cout << "initializing movie time factor (b_itbin) matrix..." << endl;
  //b_itbin = MatrixXf::Random(timeBins, trainSet.movies()) * range;
  //cout << "initializing movie time factor base (c_u) vector..." << endl;
  //c_u = VectorXf::Constant(trainSet.users(), 1) + VectorXf::Random(trainSet.users()) * range;

  //user/movie features
  cout << "initializing user feature (p_u) matrix..." << endl;
  p_u = MatrixXf::Random(features, trainSet.users()) * range;
  //cout << "initializing user feature time factor (ap_u) matrix..." << endl;
  //ap_u = MatrixXf::Random(features, trainSet.users()) * range;
  cout << "initializing user movie-factor (y_j) matrix..." << endl;
  y_j = MatrixXf::Random(features, trainSet.movies()) * range;  
  cout << "initializing movie feature (q_i) matrix..." << endl;
  q_i = MatrixXf::Random(features, trainSet.movies()) * range;

  //other
  /*
  cout << "initializing user time bias (b_ut) and movie time factor (c_ut) hash maps..." << endl;
  random_device rd;
  mt19937 gen(rd());
  uniform_real_distribution<float> dis(0, 1);
  b_ut.clear();
  c_ut.clear();
  //p_ut.clear();
  for (int u=0; u != trainSet.users(); ++u) {
    unordered_map<short int, float> map_b_ut;
    unordered_map<short int, float> map_c_ut;
    //unordered_map<short int, VectorXf> map_p_ut;
    for (vector<Datum>::const_iterator it = trainSet.userBegin(u+1); it != trainSet.userEnd(u+1); ++it) {
      int t = it->day-1;
      map_b_ut[t] = dis(gen) * range;
      map_c_ut[t] = dis(gen) * range;
      //map_p_ut[t] = VectorXf::Random(features) * 0;
    }
    b_ut.push_back(map_b_ut);
    c_ut.push_back(map_c_ut);
    //p_ut.push_back(map_p_ut);
  }
  */
}

void NetflixSVD::findMean() {
  int sum = 0;
  cout << "computing global mean..." << endl;
  for (vector<Datum>::const_iterator it = trainSet.begin(); it != trainSet.end(); ++it) {
    sum += it->rating;
  }
  mean = sum/float(trainSet.ratings());
  cout << "global mean: " << mean << endl;
}

float NetflixSVD::rmse() const {
  return rmse(trainSet);
}

float NetflixSVD::rmse(const NetflixData &d) const {
  float sum = 0;
  float e;
  cout << "computing rmse..." << endl;
  for (int u=0; u < d.users(); ++u) {
    VectorXf factorSum = sumFactors(u+1);
    for (vector<Datum>::const_iterator it = d.userBegin(u+1); it != d.userEnd(u+1); ++it) {
      if (it->rating != 0) {
	e = error(*it, factorSum);
	sum += e*e;
      }
    }
  }
  float rmse = sqrt(sum/d.ratings());
  return rmse;
}

void NetflixSVD::save(string dir) const {
  cout << "saving parameters to " << dir << endl;
  ofstream os;
  os.open((dir+"/mean.dat").c_str());
  os << mean << endl;
  os.close();
  os.open((dir+"/b_u.dat").c_str());
  os << b_u << endl;
  os.close();
  os.open((dir+"/b_i.dat").c_str());
  os << b_i << endl;
  os.close();
  os.open((dir+"/y_j.dat").c_str());
  os << y_j << endl;
  os.close();
  os.open((dir+"/p_u.dat").c_str());
  os << p_u << endl;
  os.close();
  os.open((dir+"/q_i.dat").c_str());
  os << q_i << endl;
  os.close();
  os.open((dir+"/b_itbin.dat").c_str());
  os << b_itbin << endl;
  os.close();
}

istream & operator>> (istream &is, MatrixXf &m) {
  for (int i=0; i != m.rows(); ++i) {
    for (int j=0; j != m.cols(); ++j) {
      is >> m(i, j);
    }
  }
  return is;
}

istream & operator>> (istream &is, VectorXf &v) {
  for (int i=0; i != v.size(); ++i) {
    is >> v(i);
  }
  return is;
}

void NetflixSVD::load(string dir) {
  cout << "loading parameters from " << dir << endl;
  ifstream is((dir+"/mean.dat").c_str());
  is >> mean;
  is.close();
  is.open((dir+"/b_u.dat").c_str());
  is >> b_u;
  is.close();
  is.open((dir+"/b_i.dat").c_str());
  is >> b_i;
  is.close();
  is.open((dir+"/y_j.dat").c_str());
  is >> y_j;
  is.close();
  is.open((dir+"/p_u.dat").c_str());
  is >> p_u;
  is.close();
  is.open((dir+"/q_i.dat").c_str());
  is >> q_i;
  is.close();
  is.open((dir+"/b_itbin.dat").c_str());
  is >> b_itbin;
  is.close();
}

void NetflixSVD::findMeanTimes() {
  cout << "computing time means for each user..." << endl;
  meanTimes = VectorXf::Zero(trainSet.users());
  for (int u=0; u != trainSet.users(); ++u) {
    int sum = 0;
    for (vector<Datum>::const_iterator it = trainSet.userBegin(u+1); it != trainSet.userEnd(u+1); ++it) {
      sum += it->day;
    }
    int points = trainSet.userSize(u+1);
    if (points > 0) {
      meanTimes(u) = float(sum) / points;
    }
  }
  timeExp = 0.4;
}

inline float NetflixSVD::timeDev(int user, int time) const {
  int sign = time >= meanTimes(user-1) ? 1 : -1;
  return sign * pow(abs(time - meanTimes(user-1)), timeExp);
}

inline int NetflixSVD::timeBin(int day) const {
  return timeBins * (day - 1) / (trainSet.days());
}

void NetflixSVD::savePredictions(const NetflixData &d, string filename) {
  ofstream os(filename.c_str());
  cout << "saving predictions to " << filename << "..." << endl;
  os << predict(d);
}

int NetflixSVD::iteration() {
  return iter;
}
