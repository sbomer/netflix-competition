#ifndef NETFLIX_SVD_HH
#define NETFLIX_SVD_HH

#include "NetflixData.hh"
#include <Eigen/Core>
#include <iterator>
#include <unordered_map>
#include <random>

template <typename T>
std::ostream &operator<< (std::ostream &os, const std::vector<T> &v) {
  std::ostream_iterator<T> it (os, "\n");
  copy(v.begin(), v.end(), it);
  return os;
}

struct ParamSet {

};

class NetflixSVD {

  // internal data
  const NetflixData &trainSet;
  Eigen::VectorXf meanTimes;
  int iter;  

  // fitting parameters
  float mean; //global rating mean
  //user bias
  Eigen::VectorXf b_u; //baseline user bias
  Eigen::VectorXf a_u; //time-offset bias multiplicative factor
  std::vector<std::unordered_map<short int, float> > b_ut; //daily user bias
  //movie bias
  Eigen::VectorXf b_i; //baseline movie bias
  Eigen::MatrixXf b_itbin; //timebin movie bias
  Eigen::VectorXf c_u; //user movie-bias factor baseline
  std::vector<std::unordered_map<short int, float> > c_ut; //daily user movie-bias factor
  //user features
  Eigen::MatrixXf p_u; //baseline movie features
  Eigen::MatrixXf ap_u; //feature time bias multiplicative factor
  Eigen::MatrixXf y_j; //movie user-descriptors
  // std::vector<std::unordered_map<short int, Eigen::VectorXf> > p_ut; //daily user feature bias
  //movie features
  Eigen::MatrixXf q_i; //baseline movie features

  // meta parameters
  // factor-specific learning rates, regularization
  //user bias
  float lrate_b_u, reg_b_u;
  float lrate_a_u, reg_a_u;
  float lrate_b_ut, reg_b_ut;
  //movie bias
  float lrate_b_i, reg_b_i;
  float lrate_b_itbin, reg_b_itbin;
  float lrate_c_u, reg_c_u;
  float lrate_c_ut, reg_c_ut;
  //user/movie feature
  float lrate_f, reg_f;

  float timeExp; //user time-offset sensitivity
  int timeBins; //number of bins for long-range movie time bias
  int features; //number of user/movie features

public:
  NetflixSVD(const NetflixData &trainSet);
  float predict(const Datum &d, const Eigen::VectorXf &factorSum) const;
  std::vector<float> predict(const NetflixData &d) const;
  void savePredictions(const NetflixData &d, std::string filename);
  void train();
  float error(const Datum &d, const Eigen::VectorXf &factorSum) const;
  void random(float range);
  Eigen::VectorXf sumFactors(int user) const;
  float bias(int user, int movie) const;
  void findMean();
  void findMeanTimes();
  float rmse(const NetflixData &d) const;
  float rmse() const;
  void save(std::string dir) const;
  void load(std::string dir);
  void setFeatures(int features=5);
  float timeDev(int user, int time) const;
  int timeBin(int day) const;
  void setParams(const ParamSet &p);
  int iteration();
};

#endif
