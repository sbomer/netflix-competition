#include "NetflixData.hh"

using namespace std;

bool isSmall(const Datum &d, int i) {
  return d.user <= 50000 && d.movie <= 1700;
}

bool isSmaller(const Datum &d, int i) {
  return d.user <= 5000 && d.movie <= 170;
}

bool accept(const Datum &d, int i) {
  return (i == 1 || i == 2 || i == 3) && d.user <= 50000 && d.movie <= 1700;
}

int main() {
  NetflixData set;
  set.loadFromInd("../../data/original/um/all.dta", "../../data/original/um/all.idx", accept);
  set.save("../../data/small/train.dta");
}
