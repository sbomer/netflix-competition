#include "NetflixData.hh"
#include "NetflixSVD.hh"
#include <iostream>
#include <fenv.h>

using namespace std;

int main() {

  NetflixData train("../../data/original/um/all.dta");
  NetflixData test("../../data/original/probe.dta");
  NetflixData qual("../../data/original/qual.dta");
  string dir = "../../results/svdpp_200";

  /*
    NetflixData train("../../data/small/train.dta");
    NetflixData test("../../data/small/probe.dta");
    NetflixData qual("../../data/small/qual.dta");
    string dir = "../../results/testsvd";
  */
  
  feenableexcept(FE_OVERFLOW | FE_DIVBYZERO | FE_UNDERFLOW | FE_INVALID);

  system(("mkdir -p " + dir).c_str());

  NetflixSVD svd(train);
  float outErr = svd.rmse(test);
  cout << "initial probe rmse: " << outErr << endl;
  float prevErr;
  do {
    svd.train();
    prevErr = outErr;
    outErr = svd.rmse(test);
    cout << "probe rmse: " << outErr << endl;
    if (svd.iteration() == 10) {
      svd.savePredictions(qual, dir + "/predictions_10.dat");
    }
    if (svd.iteration() == 20) {
      svd.savePredictions(qual, dir + "/predictions_20.dat");
    }
    if (svd.iteration() == 30) {
      svd.savePredictions(qual, dir + "/predictions_30.dat");
    }
    if (svd.iteration() == 40) {
      svd.savePredictions(qual, dir + "/predictions_40.dat");
    }
  } while (svd.iteration() < 50);
    //  } while (outErr < prevErr);
  cout << "converged in " << svd.iteration() << " iterations" << endl;
  svd.savePredictions(qual, dir + "/predictions.dat");
}

